\documentclass[12pt]{article}

\title{CoCo 3 512K SRAM}
\author{Ciaran Anscomb \textit{<www@6809.org.uk>}}
\date{}

\usepackage[a4paper,margin=1in]{geometry}

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00A0}{~}

\pretolerance=7500
\tolerance=2000
\emergencystretch=12pt

\usepackage{float}
\usepackage[hidelinks]{hyperref}

\usepackage[T1]{fontenc}  % T1 fonts, please
\usepackage{nimbusmono}
\usepackage{nimbusserif}

\makeatletter
\newcommand*{\textoverline}[1]{$\overline{\hbox{#1}}\m@th$}
\makeatother

\newenvironment{compactlist}
{ \begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt} }
{ \end{itemize} }


\begin{document}

\maketitle

A 512K RAM expansion design for the Tandy Colour Computer 3.  The CoCo 3 is
designed to accept such an expansion, so it's fairly straightforward.  Plenty
of boards have been produced at one time or another for this purpose.  This
design uses surface mount components and a single SRAM IC.


\subsection*{Fitting}

These instructions are common to any 512K upgrade for the CoCo 3.  First,
remove the existing socketed DRAM chips (IC16, IC17, IC18, IC19).  Cut
capacitors C65 and C66.  On a PAL CoCo 3, these capacitors are to the left of
the GIME; on an NTSC machine they are closer to the DRAM sockets.

Looking at the motherboard with the ports to the rear, CN6 is the vertically
aligned socket to the left of the original DRAM, with CN5 immediately below it
effectively forming a single 24-pin socket.  CN4 is below and to the right,
running horizontally, just above the keyboard connector.  Mount the expansion
with J1 inserted into CN6/CN5, and J2 into CN4.

The rest of this document discusses the design of the expansion.


\subsection*{Interfacing with the CoCo 3}

The RAM expansion sockets on the CoCo 3 comprise CN4, CN5 and CN6.

I couldn't find the pinouts of these connectors documented (which probably just
means I didn't search hard enough), so here's what I traced.

\begin{table}[H]
\small
\centering
\setlength\tabcolsep{4pt}
\begin{tabular}{llll}
Pin & CN4 & CN5 & CN6 \\ \hline\rule{0pt}{2.6ex}\rule[-0.9ex]{0pt}{0pt}%
1 & GND & GND & GND \\
2 & \textoverline{RAS} & D2 & +5V \\
3 & Z0 & D3 & D9 \\
4 & Z1 & D1 & D8 \\
5 & Z2 & \textoverline{WE0} & D10 \\
6 & Z3 & D0 & D11 \\
7 & Z6 & \textoverline{CAS} & D14 \\
8 & Z5 & D7 & D15 \\
9 & Z4 & D5 & D13 \\
10 & Z7 & D4 & D12 \\
11 & Z8 & D6 & \textoverline{WE1} \\
12 & GND & GND & GND
\end{tabular}
\end{table}

Standard 2.54mm pitch header strip with 0.64mm² pins plugs into these sockets
just fine; there's no need for machined pins.


\subsection*{SRAM choice}

The CoCo 3 expects to address two banks of 256K x 8bit RAM and then externally
latches 8 bits to the bus from one bank or the other for reads.  For writes it
provides two write-enable lines (\textoverline{WE0} and \textoverline{WE1}) to
select which bank is written to (the other will see a read cycle, but its data
will be ignored).

The Cypress CY62146ELL\footnote{There is also the CY62146ESL, which can operate
at lower voltages, but handily is still fine with 5V.} is a 256K x 16bit SRAM.
It has a single write-enable line (\textoverline{WE}), but two more control lines:
\textoverline{BHE} and \textoverline{BLE}, which select whether the high or low eight bits
are selected respectively.  With a little external logic, these allow a single
RAM chip to be used for the expansion.

For writes, \textoverline{WE1} and \textoverline{WE0} map directly to
\textoverline{BHE} and \textoverline{BLE} on the chip (though we also want
\textoverline{WE} to be active if either \textoverline{WE1} or
\textoverline{WE0} are).  For reads, we want both banks to be selected.  All of
these signals are active low, so this is the behaviour we want to end up with:

\begin{table}[H]
\small
\centering
\setlength\tabcolsep{4pt}
\begin{tabular}{ccccc}
\textoverline{WE1} & \textoverline{WE0} & \textoverline{WE} & \textoverline{BHE} & \textoverline{BLE} \\ \hline\rule{0pt}{2.6ex}\rule[-0.9ex]{0pt}{0pt}%
0 & 0 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 1 \\
1 & 0 & 0 & 1 & 0 \\
1 & 1 & 1 & 0 & 0
\end{tabular}
\end{table}

This translates to some very simple logic:

\begin{compactlist}
\item \textoverline{BHE} = \textoverline{WE1} \textit{AND NOT} \textoverline{WE0}
\item \textoverline{BLE} = \textit{NOT} \textoverline{WE1} \textit{AND} \textoverline{WE0}
\item \textoverline{WE} = \textoverline{WE1} \textit{AND} \textoverline{WE0}
\end{compactlist}

Two inverters and three AND gates are required.


\subsection*{Address latches}

Typically, a row address is strobed into DRAMs on \textoverline{RAS}
fall, then a column address on \textoverline{CAS} fall.  The SRAM
devices we want to use do not multiplex their addresses like this even if they
are arranged similarly internally.  Instead, they have a pin for every address
line.  Therefore we need to latch the row addresses ourselves.  The column
address can be passed through.

The row addressing requires 9 bits of latch.  I've chosen to use a 74'574 for
the first 8 bits as its pinout works better on the board than using a '273.
The tri-state capability of the '574 is not needed, so its \textoverline{OE} line is
tied low (always active).

For the ninth bit I've used a 74'1G79 to save board space.

Note that inverting versions of both ('573 and '1G80) should be perfectly fine
if there's a cost or availability reason for picking those instead.  The
address lines presented to the SRAM just have to be \textit{consistent} and
\textit{unique}.

All of these latch ICs operate on a rising clock, so we need an inverter for
the \textoverline{RAS} line.


\subsection*{Bill of materials}

Three inverters are required.  It would be natural to use a hex inverter IC
('04) and leave the other three unused, but it turns out they're needed at
opposite ends of the board.  I've instead chosen to use three individual
inverters.

In general I've chosen 74AHCT logic.  It's lower power than the 74LS logic
common in our era of computer, but will interface to TTL signals.

\begin{table}[H]
\small
\centering
\setlength\tabcolsep{4pt}
\begin{tabular}{lllll}
Component & Value & Package & Qty & Description \\ \hline\rule{0pt}{2.6ex}\rule[-0.9ex]{0pt}{0pt}%
U1,U5,U6 & 74AHCT1G04 & SOT-23 & 3 & Single inverter \\
U2 & 74AHCT08 & TSSOP-14 & 1 & Quad AND gate \\
U3 & 74AHCT574 & TSSOP-20 & 1 & Octal D-type latch \\
U4 & 74AHCT1G79 & SOT-353 & 1 & Single D-type latch \\
U7 & CY62146ESL & TSSOP-II-44 & 1 & 256Kx16bit SRAM \\
C1-C6 & 100n & 0603 & 6 & Bypass capacitors \\
J1 & 24-pin & Pin header & 1 & 2.54mm pin header \\
J2 & 12-pin & Pin header & 1 & 2.54mm pin header
\end{tabular}
\end{table}

Note: SOT-353 is also known as SC-70.


\subsection*{Similar products}

There are many!  As mentioned, the CoCo 3 was designed to accept such an
expansion, so there were several early products using DRAM, for which you can
find manuals on the
\href{https://colorcomputerarchive.com/repo/Documents/Manuals/Hardware/}{TRS-80
Color Computer
Archive}\footnote{\url{https://colorcomputerarchive.com/repo/Documents/Manuals/Hardware/}}.

There are also a few later ones doing the same job with SRAM:

\begin{itemize}
\item RETRO Innovations published a simple through-hole design called the
\href{https://github.com/go4retro/HalfMegCoCo.git}{HalfMegCoCo}\footnote{\url{https://github.com/go4retro/HalfMegCoCo.git}}.
It's based around two SRAM ICs, one per 8-bit bank.

\item Boyson Tech produced the \href{https://boysontech.com/512k-memory/}{Boomerang
Classic 512K}\footnote{\url{https://boysontech.com/512k-memory/}}.  From the
small picture, it seems like it also uses a single SRAM.  All logic appears to
be handled by a CPLD.  As Boyson Tech have left the CoCo market, it's unclear
who will now produce these.

\item Cloud-9 makes the
\href{http://www.frontiernet.net/~mmarlette/Cloud-9/Hardware/512K.html}{Triad
512K SRAM
Upgrade}\footnote{\url{http://www.frontiernet.net/~mmarlette/Cloud-9/Hardware/512K.html}}.
It's a little larger, but surface mount and again based around two SRAM ICs.
\end{itemize}


\subsection*{Further development}

Jim Brain from RETRO Innovations has observed that the logic could be
simplified further.  If you assume that \textoverline{WE1} and
\textoverline{WE0} will never be active (low) at the same time, you get a
slightly simpler truth table (where 'X' means "don't care"):

\begin{table}[H]
\small
\centering
\setlength\tabcolsep{4pt}
\begin{tabular}{cccccc}
\textoverline{WE1} & \textoverline{WE0} & \textoverline{WE} & \textoverline{BHE} & \textoverline{BLE} & Action \\ \hline\rule{0pt}{2.6ex}\rule[-0.9ex]{0pt}{0pt}%
0 & 0 & X & X & X & Invalid \\
0 & 1 & 0 & 0 & 1 & Write high byte \\
1 & 0 & 0 & 1 & 0 & Write low byte \\
1 & 1 & 1 & 0 & 0 & Read both bytes
\end{tabular}
\end{table}

Which means:

\begin{compactlist}
\item \textoverline{BLE} = \textit{NOT} \textoverline{WE1}
\item \textoverline{BHE} = \textit{NOT} \textoverline{WE0}
\item \textoverline{WE} = \textoverline{WE1} \textit{AND} \textoverline{WE0}
\end{compactlist}

Which now only requires one \textit{AND} gate, and could be realised with a
(smaller) 74'1G08.  Furthermore, the whole thing can be implemented using
\textit{XOR} gates:

\begin{compactlist}
\item \textoverline{BLE} = \textoverline{WE1} \textit{XOR} 1
\item \textoverline{BHE} = \textoverline{WE0} \textit{XOR} 1
\item \textoverline{WE} = \textit{NOT} ( \textoverline{WE1} \textit{XOR} \textoverline{WE0} )
\end{compactlist}
which is equivalent to:
\begin{compactlist}
\item \textoverline{WE} = \textoverline{WE1} \textit{XOR} ( \textit{NOT} \textoverline{WE0} )
\end{compactlist}
where we can substitute \textit{NOT} \textoverline{WE0} with the above \textoverline{BHE}
providing:
\begin{compactlist}
\item \textoverline{WE} = \textoverline{WE1} \textit{XOR} \textoverline{BHE}
\end{compactlist}

Which means it can all be implemented using three gates from a quad
\textit{XOR} (74'86).  This also leaves the last gate free to implement the
inverter for \textoverline{RAS} (albeit requiring that signal to be routed a
little further). And, when using \textit{XOR} gates to realize the logic, they actually take care of the invalid state.  If \textoverline{WE0} and \textoverline{WE1} are both 0, not only will \textoverline{BHE} and \textoverline{BLE} both be 1 (inverted from the original signals), but \textoverline{WE} will be 1 \textit{XOR} 0, or 1, or inactive.

Thanks, Jim!


\subsection*{License}

Released under the Creative Commons Attribution-ShareAlike 4.0 International
(CC BY-SA 4.0).  Full text in the \texttt{LICENSE} file.

You are free to:

\begin{itemize}
\item \textbf{Share} --- copy and redistribute the material in any medium or
format
\item \textbf{Adapt} --- remix, transform, and build upon the material for any purpose,
even commercially.
\end{itemize}

The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

\begin{itemize}
\item \textbf{Attribution} --- You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

\item \textbf{ShareAlike} --- If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
\end{itemize}

\textbf{No additional restrictions} --- You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.


\subsection*{Revision history}

\textbf{2021-03-30} Updated text clarifying which signals are active low \\
\textbf{2021-03-28} Traced expansion connector signals through to the GIME (prompted by Jim Brain) \\
\textbf{2021-03-27} New \textit{Further development} section (information from Jim Brain)


\end{document}
