# CoCo 3 512K SRAM

A 512K RAM expansion design for the Tandy Colour Computer 3. The CoCo 3
is designed to accept such an expansion, so it's fairly straightforward.
Plenty of boards been produced at one time or another for this purpose.
This design uses surface mount components and a single SRAM IC.

Check the [PDF documentation](http://www.6809.org.uk/dragon/cc3-512k.pdf) for more details.

Or here's the [schematic as a PDF](http://www.6809.org.uk/dragon/cc3-512k-sch.pdf).
